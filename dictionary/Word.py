class Word:
    def __init__(self, type, mean):
        self.type = type
        self.mean = mean

    def printWord(self):
        print("(" + self.type + ") " + self.mean)
