from dictionary.DictionaryItem import DictionaryItem
# library for read and write binary file
import pickle

# initialize a list dictionary
thisdict = []


# check if word exist
def checkExistWord(word):
    for x in thisdict:
        if x.word == word:
            return 1
    return 0


def printAll():
    print("List all word in a dictionary")
    for x in thisdict:
        x.print()


# check and open a file
try:
    thisdict = pickle.load(open("file.pkl", "rb"))
except EOFError:
    thisdict = []

while 1:
    print("1.Add a new word to dictionary")
    print("2.Sort dictionary with alphabet sort")
    print("3.Search a word's meaning")
    print("4.Delete a word")
    print("5.Show all dictionary")
    print("6.Edit meaning of a word")
    print("0.Exit")
    while 1:
        choose = int(input("Type your choose from 0-6: "))
        if choose >= 0 or choose <= 6:
            break
    if choose == 1:
        newWord = input("Enter new word: ")
        newType = input("Enter new type: ")
        newMeaning = input("Enter new meaning ")
        if checkExistWord(newWord) == 0:
            thisdict.append(DictionaryItem(newWord, newType, newMeaning))
        else:
            for x in thisdict:
                if x.word == newWord:
                    x.addItem(newType, newMeaning)
    if choose == 2:
        thisdict.sort(key=lambda x: x.word)
    if choose == 3:
        word = input("Type some word: ")
        for x in thisdict:
            if x.word == word:
                print("Text found: ")
                x.print()
    if choose == 4:
        isValid = 0
        word = input("Type a word to delete: ")
        for x in thisdict:
            if x.word == word:
                isValid = 1
                thisdict.remove(x)
        if isValid == 0:
            print("Not found this word!")
    if choose == 5:
        printAll()
    if choose == 6:
        word = input("Type a word to edit meaning: ")
        for x in thisdict:
            if x.word == word:
                x.print()
                newType = input("Enter new type: ")
                newMeaning = input("Enter new meaning ")
                x.addItem(newType , newMeaning)
    # open and write to binary file
    with open('file.pkl', 'wb') as output:
        pickle.dump(thisdict, output, pickle.HIGHEST_PROTOCOL)
    if choose == 0:
        break
